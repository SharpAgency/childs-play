//
//  KTAOutdoorViewController.m
//  KidsTripAdvisor
//
//  Created by Maris on 9/09/13.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "KTAListViewController.h"
#import "KTACustomUITableViewCell.h"
#import "KTADetailViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
#define ChildsPlayJsonURL [NSURL URLWithString:@"http://www.testdev.com.au/kta/downloadJSON.php"]

@interface KTAListViewController () {
    NSMutableArray *detailTextArray;
    NSIndexPath *detailIndexPath;    
    
    NSMutableArray *pointsOnMap;
    
    NSMutableArray *unsortedLocations;
    NSArray *sortedLocations;
}

@end

@implementation KTAListViewController

- (void)viewWillAppear:(BOOL)animated {
    
    // update users location.
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
    
    [self getData];
    
    self.tableView.backgroundColor = [UIColor colorWithWhite:0.8 alpha:1.0];
    self.navigationController.navigationBar.translucent = NO;
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    _iAd.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"iAd.png"]];

    pointsOnMap = [[NSMutableArray alloc] init];
}

- (void)viewDidLoad
{
    // delegates for tableview are set in the storyboard
    [super viewDidLoad];
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    
    // Hide the search bar until user scrolls up
    CGRect newBounds = self.tableView.bounds;
    newBounds.origin.y = newBounds.origin.y + self.searchBar.bounds.size.height;
    self.tableView.bounds = newBounds;
    
    // When view loads, stop updating location. Saves on Battery. Coming back to this view will refresh it.
    [locationManager stopUpdatingLocation];
    
    // initialize filtered array
    self.filteredContent = [NSMutableArray arrayWithCapacity:[self.content count]];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return [self.filteredContent count];
    } else {
        return [sortedLocations count];
    }
}

- (KTACustomUITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *newContent;
    if (tableView == self.searchDisplayController.searchResultsTableView) newContent = [self.filteredContent objectAtIndex:indexPath.row];
    else newContent = [sortedLocations objectAtIndex:indexPath.row];

    // This is also set on the cell in the storyboard.
    static NSString *CellIdentifier = @"Cell";
    KTACustomUITableViewCell *cell = (KTACustomUITableViewCell *)[self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[KTACustomUITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle  reuseIdentifier:CellIdentifier];
    }
    // Set location name from plist
    cell.titleTextLabel.text = [newContent valueForKey:@"CompanyName"];
    
    // set category label
    cell.categoryTextLabel.text = [NSString stringWithFormat:@"%@", [newContent valueForKey:@"GroupClassification"]];
    
    // set suburb label
    cell.suburbTextLabel.text = [NSString stringWithFormat:@"%@", [newContent valueForKey:@"City"]];
    
    // create string of location in KM (default is Meters)
    cell.distanceTextLabel.text = [newContent valueForKey:@"DistanceForSort"];

    // set image
    [cell.imageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://www.testdev.com.au/kta/appimgs/%@/front.png", [newContent valueForKey:@"store_id"]]] placeholderImage:[UIImage imageNamed:@"NoImageIcon"]];
    
    // Extra customizeation
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];

    // set cell background image
    UIImageView *cellImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cellBackgroundImage"]];
    [cell setBackgroundView:cellImage];
    
    // Check if premium
    if ([[newContent valueForKey:@"Premium"] isEqualToString:@"YES"]) {
        cell.isPremium = YES;
    }
       NSLog(@"%@", newContent);

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // perform segue to detail view
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        
        [self performSegueWithIdentifier:@"detailViewSegue" sender:tableView];
    }
}
#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {    
    if ([[segue identifier] isEqualToString:@"detailViewSegue"]) {
        KTADetailViewController *vc = [segue destinationViewController];
        
        if (sender == self.searchDisplayController.searchResultsTableView) {
            NSIndexPath *indexPath = [self.searchDisplayController.searchResultsTableView indexPathForSelectedRow];
            [vc setContentDictionary:[self.content objectAtIndex:indexPath.row]];
            
        } else {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
            [vc setContentDictionary:[self.content objectAtIndex:indexPath.row]];
        
        }
    }
}

- (void)filterContentForSearchText:(NSString *)searchText scope:(NSString *)scope {
    // Update the filtered array based on the search text and scope
    // remove all objects from the filtered array
    [self.filteredContent removeAllObjects];
    NSSortDescriptor *sortAlpha = [NSSortDescriptor sortDescriptorWithKey:@"CompanyName" ascending:YES];
    
    // Further order the array with the scope
    if ([scope isEqualToString:@"All"]) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.CompanyName contains[c]%@ OR SELF.Keywords contains[c]%@ OR SELF.GroupClassification contains[c]%@ OR SELF.Keywords contains[c]%@", searchText, searchText, searchText, searchText];
        self.filteredContent = [NSMutableArray arrayWithArray:[sortedLocations filteredArrayUsingPredicate:predicate]];
        [self.filteredContent sortUsingDescriptors:[NSArray arrayWithObject:sortAlpha]];
        
    } else if ([scope isEqualToString:@"Suburb"]) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.City contains[c]%@", searchText];
        self.filteredContent = [NSMutableArray arrayWithArray:[sortedLocations filteredArrayUsingPredicate:predicate]];
    } else if ([scope isEqualToString:@"Postcode"]) {
        NSString *inputString = searchText;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.PostCode contains[c] %@", inputString];
        self.filteredContent = [NSMutableArray arrayWithArray:[sortedLocations filteredArrayUsingPredicate:predicate]];
    }
    
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    // tells the table data source to reload when text changes
    [self filterContentForSearchText:searchString scope:[[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    // Res YES to cause the search result table view to be reloaded.
    return YES;
}
- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption {
    //  Tells the table data source to reload when scope bar selection changes
    [self filterContentForSearchText:self.searchDisplayController.searchBar.text scope:[[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]];
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

- (void)searchDisplayController:(UISearchDisplayController *)controller willShowSearchResultsTableView:(UITableView *)tableView {
    [tableView setRowHeight:[[self tableView] rowHeight]];
    [tableView reloadData];
}

- (void)getData {
    // init unsorted array
    unsortedLocations = [[NSMutableArray alloc] initWithCapacity:5000];
    
    // start at row 0
    int index = 0;
    
    // loop through each location and calculate distance from current location
    for (NSDictionary *location in self.content) {
        // get the coordinates from current location
        CLLocation *currentLocation = [[CLLocation alloc] initWithLatitude:locationManager.location.coordinate.latitude longitude:locationManager.location.coordinate.longitude];
        
        // get the coordinates for place location at row
        double lat1 = [[[self.content objectAtIndex:index] valueForKey:@"Latitude"] doubleValue];
        double lon1 = [[[self.content objectAtIndex:index] valueForKey:@"Longitude"] doubleValue];
            
        CLLocation *placeLocation = [[CLLocation alloc] initWithLatitude:lat1 longitude:lon1];
        
        // calculate the distance from each location to users location and format it in kilometers
        CLLocationDistance dist = ([placeLocation distanceFromLocation:currentLocation]/1000);
        
        // define the distance for display
        NSNumber *distanceForDict = [NSNumber numberWithDouble:dist];
        
        // define the distance for sorting
        NSString *distanceForSort = [NSString stringWithFormat:@"%4.2f", dist];
        
        
        // create a dictionary for each location and define the values for each key, adding those for distance
        NSMutableDictionary *locationDict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                             [[self.content objectAtIndex:index] objectForKey:@"store_id"], @"store_id",
                                             [[self.content objectAtIndex:index] objectForKey:@"CompanyName"], @"CompanyName",
                                             [[self.content objectAtIndex:index] objectForKey:@"Address"], @"Address",
                                             [[self.content objectAtIndex:index] objectForKey:@"City"], @"City",
                                             [[self.content objectAtIndex:index] objectForKey:@"PostCode"], @"PostCode",
                                             [[self.content objectAtIndex:index] objectForKey:@"State"], @"State",
                                             [[self.content objectAtIndex:index] objectForKey:@"Telephone1"], @"Telephone1",
                                             [[self.content objectAtIndex:index] objectForKey:@"EmailAddress"], @"EmailAddress",
                                             [[self.content objectAtIndex:index] objectForKey:@"WebSite"], @"WebSite",
                                             [[self.content objectAtIndex:index] objectForKey:@"GroupClassification"], @"GroupClassification",
                                             [[self.content objectAtIndex:index] objectForKey:@"Keywords"], @"Keywords",
                                             [[self.content objectAtIndex:index] objectForKey:@"Type"], @"Type",
                                             [[self.content objectAtIndex:index] objectForKey:@"Latitude"], @"Latitude",
                                             [[self.content objectAtIndex:index] objectForKey:@"Longitude"], @"Longitude",
                                             [[self.content objectAtIndex:index] objectForKey:@"LongDescription"], @"LongDescription",
                                             distanceForDict, @"DistanceForDict",
                                             distanceForSort, @"DistanceForSort", nil];
        
        // add dictionary to the unsorted array
        [unsortedLocations addObject:locationDict];
        
        // iterate through the list of locations
        ++index;
    }
    
    // Create the sorting element
    NSSortDescriptor *sortDescriptor;
    
    // define what to sort by
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"DistanceForDict" ascending:YES];
    
    // build new array with those elements to be sorted
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    // create new array based on the sorted value
    sortedLocations = [[NSArray alloc] initWithArray:[unsortedLocations sortedArrayUsingDescriptors:sortDescriptors]];
    
    [self.tableView reloadData];
}



@end
