//
//  KTACustomStoryboardSegue.m
//  KidsTripAdvisor
//
//  Created by Maris on 26/09/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "KTACustomStoryboardSegue.h"

@implementation KTACustomStoryboardSegue

-(void)perform
{
    __block UIViewController *sourceViewController = (UIViewController*)[self sourceViewController];
    __block UIViewController *destinationController = (UIViewController*)[self destinationViewController];
    
    [UIView transitionWithView:sourceViewController.navigationController.view duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^
     {
         [sourceViewController.navigationController pushViewController:destinationController animated:NO];
     }
                    completion:^(BOOL finished)
     {
         NSLog(@"Transition Completed");
     }];
}
@end
