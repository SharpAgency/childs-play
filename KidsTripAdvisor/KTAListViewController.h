//
//  KTAOutdoorViewController.h
//  KidsTripAdvisor
//
//  Created by Maris on 9/09/13.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface KTAListViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, CLLocationManagerDelegate, UISearchBarDelegate, UISearchDisplayDelegate> {
    CLLocationManager *locationManager;
}
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *content;

@property (strong, nonatomic) NSMutableArray *filteredContent;

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property (weak, nonatomic) IBOutlet UILabel *iAd;

@end



