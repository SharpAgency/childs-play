//
//  KTAFavoritesViewController.m
//  KidsTripAdvisor
//
//  Created by Maris on 9/09/13.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "KTAFavoritesViewController.h"
#import "KTADetailViewController.h"
#import "KTACustomUITableViewCell.h"

#import <SDWebImage/UIImageView+WebCache.h>

@interface KTAFavoritesViewController () {
    NSMutableArray *favoritesArray;
    CLLocationManager *locationManager;
}

@end

@implementation KTAFavoritesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // Delegate set in IB
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    
    _iAd.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"iAd.png"]];
    
    // When view loads, stop updating location. Saves on Battery. Coming back to this view will refresh it.
    [locationManager stopUpdatingLocation];
    
}

- (void)viewWillAppear:(BOOL)animated {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"favorites.plist"];
    favoritesArray = [[NSMutableArray alloc] initWithContentsOfFile:path];
    
    // update users location.
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
    
    self.tableView.backgroundColor = [UIColor colorWithWhite:0.8 alpha:1.0];
    self.navigationController.navigationBar.translucent = NO;
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Search and load document
    return [favoritesArray count];
}

- (KTACustomUITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSArray *newContent;
    if (tableView == self.searchDisplayController.searchResultsTableView) newContent = [favoritesArray objectAtIndex:indexPath.row];
    else newContent = [favoritesArray objectAtIndex:indexPath.row];
    
    // This is also set on the cell in the storyboard.
    static NSString *CellIdentifier = @"Cell";
    KTACustomUITableViewCell *cell = (KTACustomUITableViewCell *)[self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[KTACustomUITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle  reuseIdentifier:CellIdentifier];
    }
    // Set location name from plist
    cell.titleTextLabel.text = [newContent valueForKey:@"CompanyName"];
    
    // set category label
    cell.categoryTextLabel.text = [NSString stringWithFormat:@"%@", [newContent valueForKey:@"GroupClassification"]];
    
    // set suburb label
    cell.suburbTextLabel.text = [NSString stringWithFormat:@"%@", [newContent valueForKey:@"City"]];
    
    // create string of location in KM (default is Meters)
    cell.distanceTextLabel.text = [newContent valueForKey:@"DistanceForSort"];
    
    // set image
    [cell.imageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://www.testdev.com.au/kta/appimgs/%@/front.png", [newContent valueForKey:@"store_id"]]] placeholderImage:[UIImage imageNamed:@"NoImageIcon"]];
    
    // Extra customizeation
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    
    // set cell background image
    UIImageView *cellImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cellBackgroundImage"]];
    [cell setBackgroundView:cellImage];
    
    // Check if premium
    if ([[newContent valueForKey:@"Premium"] isEqualToString:@"YES"]) {
        cell.isPremium = YES;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Perform segue to detail view
    [self performSegueWithIdentifier:@"detailViewSegue" sender:tableView];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
    [super setEditing:editing animated:animated];
    if (editing == YES) {
        NSLog(@"editing");
        
    } else {
        NSLog(@"not editing");
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [favoritesArray removeObjectAtIndex:indexPath.row];
    }
    // Fetch favorites plist with data
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    NSString *path = [documentDirectory stringByAppendingPathComponent:@"favorites.plist"];
    
    // Write data to favorites plist
    [favoritesArray writeToFile:path atomically:YES];
    [tableView reloadData];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"detailViewSegue"]) {
        KTADetailViewController *vc = [segue destinationViewController];
        
        if (sender == self.searchDisplayController.searchResultsTableView) {
            NSIndexPath *indexPath = [self.searchDisplayController.searchResultsTableView indexPathForSelectedRow];
            [vc setContentDictionary:[favoritesArray objectAtIndex:indexPath.row]];
            
        } else {
            NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
            [vc setContentDictionary:[favoritesArray objectAtIndex:indexPath.row]];
            
        }
    }
}

@end
