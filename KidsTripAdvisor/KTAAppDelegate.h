//
//  KTAAppDelegate.h
//  KidsTripAdvisor
//
//  Created by Maris on 9/09/13.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KTAAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navController;

@end
