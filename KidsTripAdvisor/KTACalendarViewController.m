//
//  KTACalendarViewController.m
//  KidsTripAdvisor
//
//  Created by Beau Young on 27/09/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "KTACalendarViewController.h"
#import <EventKit/EventKit.h>

@interface KTACalendarViewController () {
    NSString *locationName;
    NSString *suburb;
    NSString *dateSelected;
    
    NSDate *dateForCalendar;
}

@end

@implementation KTACalendarViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
	// Do any additional setup after loading the view.
    [self.datePickerView addTarget:self action:@selector(updateTextField:) forControlEvents:UIControlEventValueChanged];
    [self.dateTextField setInputView:self.datePickerView];
    
    locationName = [_singleDetails valueForKey:@"CompanyName"];
    suburb = [_singleDetails valueForKey:@"City"];
    
    [_locationNameLabel setText:locationName];
    [_addressLabel setText:suburb];
    
    [_datePickerView setMinimumDate:[NSDate date]];
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
}

-(void)updateTextField:(id)sender
{
    UIDatePicker *picker = (UIDatePicker*)self.dateTextField.inputView;
    
    NSLocale *locale = [NSLocale currentLocale];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSString *dateFormat = [NSDateFormatter dateFormatFromTemplate:@"E MMM d yyyy h m a" options:0 locale:locale];
    [formatter setDateFormat:dateFormat];
    [formatter setLocale:locale];
    NSLog(@"Formatted date: %@", [formatter stringFromDate:picker.date]);
    
    dateForCalendar = picker.date;
    self.dateTextField.text = [NSString stringWithFormat:@"%@", [formatter stringFromDate:picker.date]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)addToCalendarTapped:(UIButton *)sender {
    
    EKEventStore *store = [[EKEventStore alloc] init];
    [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
        if (!granted) { return; }
        EKEvent *event = [EKEvent eventWithEventStore:store];
        event.title = locationName;
        event.startDate = dateForCalendar; //today
        event.location = suburb;
        event.endDate = [event.startDate dateByAddingTimeInterval:60*60];  //set 1 hour meeting
        [event setCalendar:[store defaultCalendarForNewEvents]];
        NSError *err = nil;
        [store saveEvent:event span:EKSpanThisEvent commit:YES error:&err];
//        NSString *savedEventId = event.eventIdentifier;  //this is so you can access this event later
    }];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success!" message:[NSString stringWithFormat:@"The event \"%@\" was added to your calendar", locationName] delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
    [alert show];
}

- (IBAction)cancelBtnTapped:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}




@end
