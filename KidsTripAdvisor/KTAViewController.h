//
//  KTAViewController.h
//  KidsTripAdvisor
//
//  Created by Maris on 9/09/13.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>


@interface KTAViewController : UIViewController <CLLocationManagerDelegate> {
    CLLocationManager *locationManager;
    NSMutableArray *outdoorContent;
    NSMutableArray *indoorContent;

}

@property (strong, nonatomic) NSMutableArray *allContent;

@property (weak, nonatomic) IBOutlet UIView *mainView;

@property (weak, nonatomic) IBOutlet UIButton *myCalendarBtn;

@property (weak, nonatomic) IBOutlet UILabel *iAd;

@property (strong, nonatomic) NSString *state;

@property BOOL isState;
@property BOOL isPostcode;
@property BOOL isLocation;

@end
