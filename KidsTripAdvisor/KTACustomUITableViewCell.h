//
//  KTACustomUITableViewCell.h
//  KidsTripAdvisor
//
//  Created by Maris on 10/09/13.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface KTACustomUITableViewCell : UITableViewCell

@property (assign, nonatomic) BOOL isPremium;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (weak, nonatomic) IBOutlet UILabel *titleTextLabel;

@property (weak, nonatomic) IBOutlet UILabel *categoryTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *suburbTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceTextLabel;

@end
