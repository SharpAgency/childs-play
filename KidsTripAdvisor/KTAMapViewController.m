//
//  KTADisclaimerViewController.m
//  KidsTripAdvisor
//
//  Created by Maris on 18/09/2013.
//  Copyright (c) 2013 Sharp Agency. All rights reserved.
//

#import "KTAMapViewController.h"
#import "KTAViewController.h"

#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
#define ChildsPlayJsonURL [NSURL URLWithString:@"http://www.testdev.com.au/kta/downloadJSON.php?state=QLD"]
#define ChildPlayJsonURLstring @"http://www.testdev.com.au/kta/downloadJSON.php"


@interface KTAMapViewController () {
    NSMutableArray *content;
    NSString *stateSelected;
    NSString *postcodeSelected;
    
    float lat;
    float lon;
}

@end

@implementation KTAMapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.postcodeTextField.delegate = self;
    
	// Do any additional setup after loading the view.
    // set background depending on device screen size
    UIImage *backgroundImage = [[UIImage alloc] init];
    if ([UIScreen mainScreen].bounds.size.height > 480.0f) backgroundImage = [UIImage imageNamed:@"iphone5Background.jpg"];
    else backgroundImage = [UIImage imageNamed:@"iphoneBackground.jpg"];
    
    self.mainView.backgroundColor = [UIColor colorWithPatternImage:backgroundImage];
    
    // locationManager update as location
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
    [locationManager stopUpdatingLocation];
    CLLocation *location = [locationManager location];
    // Configure the new event with information from the location
    
    lon = location.coordinate.longitude;
    lat = location.coordinate.latitude;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    KTAViewController *vc = [segue destinationViewController];
    
    if ([[segue identifier] isEqualToString:@"QLDSegue"]) {
        
        vc.isState = YES;
        [self createContentForState:@"?state=QLD"];
        [vc setAllContent:content];
        [vc setState:@"QLD"];
        [vc setTitle:@"QLD"];
    }
    else if ([[segue identifier] isEqualToString:@"VICSegue"]) {
        vc.isState = YES;
        [self createContentForState:@"?state=VIC"];
        [vc setAllContent:content];
        [vc setState:@"VIC"];
        [vc setTitle:@"VIC"];
    }
    else if ([[segue identifier] isEqualToString:@"TASSegue"]) {
        vc.isState = YES;
        
        [self createContentForState:@"?state=TAS"];
        [vc setAllContent:content];
        [vc setState:@"TAS"];
        [vc setTitle:@"TAS"];
    }
    else if ([[segue identifier] isEqualToString:@"NTSegue"]) {
        vc.isState = YES;
        [self createContentForState:@"?state=NT"];
        [vc setAllContent:content];
        [vc setState:@"NT"];
        [vc setTitle:@"NT"];
    }
    else if ([[segue identifier] isEqualToString:@"ACTSegue"]) {
        vc.isState = YES;
        [self createContentForState:@"?state=ACT"];
        [vc setAllContent:content];
        [vc setState:@"ACT"];
        [vc setTitle:@"ACT"];
    }
    else if ([[segue identifier] isEqualToString:@"SASegue"]) {
        vc.isState = YES;
        [self createContentForState:@"?state=SA"];
        [vc setAllContent:content];
        [vc setState:@"SA"];
        [vc setTitle:@"SA"];
    }
    else if ([[segue identifier] isEqualToString:@"WASegue"]) {
        vc.isState = YES;
        [self createContentForState:@"?state=WA"];
        [vc setAllContent:content];
        [vc setState:@"WA"];
        [vc setTitle:@"WA"];
    }
    else if ([[segue identifier] isEqualToString:@"NSWSegue"]) {
        vc.isState = YES;
        [self createContentForState:@"?state=NSW"];
        [vc setAllContent:content];
        [vc setState:@"NSW"];
        [vc setTitle:@"NSW"];
    }
    else if ([[segue identifier] isEqualToString:@"nearmeSegue"]) {
        vc.isLocation = YES;
        [self createContentWithLatitiude:lat longitude:lon];
        [vc setAllContent:content];
        [vc setTitle:@"Near me"];
    }
    else if ([[segue identifier] isEqualToString:@"postcodeSegue"]) {
        vc.isPostcode = YES;
        [self createContentWithPostcode:self.postcodeTextField.text];
        [self.postcodeTextField resignFirstResponder];
        self.postcodeTextField.text = nil;
        self.searchBtn.hidden = YES;
        [vc setAllContent:content];
        [vc setTitle:@"Postcode"];
    }
}

// Takes a state and adds it onto the pre defined URL, then calls the "fetchedData" method
- (void)createContentForState:(NSString *)state {
    
    NSURL *newURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", ChildPlayJsonURLstring, state]];
    NSData *data = [NSData dataWithContentsOfURL:newURL];
    [self performSelectorOnMainThread:@selector(fetchedData:)
                           withObject:data
                        waitUntilDone:YES];
}


- (void)createContentWithPostcode:(NSString *)postcode {
    NSURL *newURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@?postcode=%@", ChildPlayJsonURLstring, postcode]];
    NSData *data = [NSData dataWithContentsOfURL:newURL];
    [self performSelectorOnMainThread:@selector(fetchedData:)
                           withObject:data
                        waitUntilDone:YES];
}


- (void)createContentWithLatitiude:(double)latitude longitude:(double)longitude {
    
    NSURL *newURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@?loc=%f,%f", ChildPlayJsonURLstring, latitude, longitude]];
    NSData *data = [NSData dataWithContentsOfURL:newURL];
    [self performSelectorOnMainThread:@selector(fetchedData:)
                           withObject:data
                        waitUntilDone:YES];
}

// Takes in postcode of users location
#pragma mark - JSON Parsing method
- (void)fetchedData:(NSData *)responseData {
    // parse out the json data
    NSError *error;
    
    content = [NSJSONSerialization JSONObjectWithData:responseData
                                              options:kNilOptions
                                                error:&error];
    
}

#pragma mark - TextField Delegate / Protocl Methods
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    self.searchBtn.hidden = NO;
}

- (void)textDidChange:(id<UITextInput>)textInput {
    
}

- (void)selectionDidChange:(id<UITextInput>)textInput {
    
}

- (void)selectionWillChange:(id<UITextInput>)textInput {
    
}

- (void)textWillChange:(id<UITextInput>)textInput{
    
}


@end
